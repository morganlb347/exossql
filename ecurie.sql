5)
SELECT *
FROM poney

6) SELECT  nom 
    FROM poney 


7)
SELECT poids 
FROM poney
WHERE
nom='Petit tonnerre'

8)
SELECT COUNT(*) as "nombre de poney"
FROM poney

9)
SELECT Distinct(P.idbox)
FROM box b
Right join poney p on b.idbox=p.idbox


10)
SELECT Distinct(B.idbox)
FROM box b
LEFT join poney p on b.idbox=p.idbox
WHERE
p.idbox is null


11)
SELECT  numlot,quantite,datereception,nom
FROM receptionfoin r 
INNER JOIN typefoin t on r.idtypefoin=t.idtypefoin

12)
SELECT nom
FROM salarie s 
LEFT join tranfert t on s.idsalarie=t.idsalarie
WHERE
t.idsalarie is null

13)
SELECT r.*
FROM receptionfoin r 
LEFT join tranfert t on r.numlot=t.numlot
WHERE
t.numlot is null

14)


CREATE VIEW quantitetranfert AS
SELECT numlot,sum(quantite) as quantitetranfert
FROM tranfert t 
GROUP by t.numlot


SELECT r.*
FROM receptionfoin r 
LEFT join quantitetranfert t on r.numlot=t.numlot
WHERE
t.quantitetranfert=r.quantite


15)
SELECT nom,sum(quantite) as 'quantite totale'
FROM salarie s 
INNER join tranfert t on t.idsalarie=s.idsalarie
GROUP by s.idsalarie


16)
SELECT B.idbox
FROM box b
INNER join tranfert t on t.idbox=b.idbox
INNER join receptionfoin f on t.numlot=f.numlot
INNER join typefoin ty on f.idtypefoin=ty.idtypefoin
WHERE
ty.nom='foin de graminées'


17)
SELECT s.nom 
FROM salarie s
INNER join tranfert t on t.idsalarie=s.idsalarie
INNER join  box b  on t.idbox=b.idbox
INNER join poney p on b.idbox=p.idbox
WHERE
p.nom='Petit tonnerre'




