-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 01 déc. 2022 à 13:19
-- Version du serveur : 5.7.24
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `confostock`
--

-- --------------------------------------------------------

--
-- Structure de la table `acheteur`
--

DROP TABLE IF EXISTS `acheteur`;
CREATE TABLE IF NOT EXISTS `acheteur` (
  `idacheteur` int(11) NOT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `nomenteprise` varchar(50) DEFAULT NULL,
  `numerotel` int(11) DEFAULT NULL,
  `numerofax` int(11) DEFAULT NULL,
  PRIMARY KEY (`idacheteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `acheteur`
--

INSERT INTO `acheteur` (`idacheteur`, `ville`, `nomenteprise`, `numerotel`, `numerofax`) VALUES
(1, ' Paris', 'ToutABasPrix', 145699863, 145896632),
(2, 'Brest', 'ToutConfort', 245896325, 245879635),
(3, 'Lannion ', 'DepotStock', 245879632, 245789631),
(4, 'Guingamp', 'ConfADonf ', 254698865, 278965423),
(5, 'paris', 'Askor', 245896329, 245879641),
(6, 'plerin', 'ToutEnGris', 145699863, 245789631);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `idacheteur` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  PRIMARY KEY (`idacheteur`,`idproduit`),
  KEY `idproduit` (`idproduit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`idacheteur`, `idproduit`, `quantite`) VALUES
(1, 1, 3),
(1, 2, 2),
(1, 3, 12),
(2, 1, 1),
(2, 4, 2),
(2, 5, 2),
(2, 6, 3),
(3, 3, 12),
(3, 7, 4),
(3, 9, 5),
(4, 1, 1),
(4, 2, 1),
(4, 4, 7),
(4, 8, 4),
(4, 9, 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `lesproduitcommande`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `lesproduitcommande`;
CREATE TABLE IF NOT EXISTS `lesproduitcommande` (
`nom` varchar(50)
,`quantite` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `prepacommande`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `prepacommande`;
CREATE TABLE IF NOT EXISTS `prepacommande` (
`nom` varchar(50)
,`nbproduitprepa` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure de la table `preparateur`
--

DROP TABLE IF EXISTS `preparateur`;
CREATE TABLE IF NOT EXISTS `preparateur` (
  `idprepa` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idprepa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `preparateur`
--

INSERT INTO `preparateur` (`idprepa`, `nom`) VALUES
(1, 'Jules'),
(2, 'Alice'),
(3, 'Betty'),
(4, 'Billy'),
(5, 'Maurice');

-- --------------------------------------------------------

--
-- Structure de la table `preparation`
--

DROP TABLE IF EXISTS `preparation`;
CREATE TABLE IF NOT EXISTS `preparation` (
  `idprepa` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  PRIMARY KEY (`idprepa`,`idproduit`),
  KEY `idproduit` (`idproduit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `preparation`
--

INSERT INTO `preparation` (`idprepa`, `idproduit`) VALUES
(1, 1),
(5, 1),
(1, 2),
(5, 2),
(1, 3),
(5, 3),
(2, 4),
(5, 4),
(2, 5),
(5, 5),
(2, 6),
(5, 6),
(4, 7),
(5, 7),
(4, 8),
(5, 8),
(4, 9),
(5, 9);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `idproduit` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idproduit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`idproduit`, `nom`) VALUES
(1, 'canapé lit'),
(2, 'table basse'),
(3, 'chaise'),
(4, 'bureau'),
(5, 'chaise de bureau'),
(6, 'commode'),
(7, 'dressing '),
(8, 'table'),
(9, 'lit '),
(10, 'table basse1');

-- --------------------------------------------------------

--
-- Structure de la table `registre`
--

DROP TABLE IF EXISTS `registre`;
CREATE TABLE IF NOT EXISTS `registre` (
  `id` int(11) NOT NULL,
  `dateprepa` date DEFAULT NULL,
  `idproduit` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idproduit` (`idproduit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `registre`
--

INSERT INTO `registre` (`id`, `dateprepa`, `idproduit`) VALUES
(1, '2022-12-08', 1),
(2, '2022-12-21', 2),
(3, '2022-12-14', 3),
(4, '2022-12-20', 4),
(5, '2022-12-24', 5),
(6, '2022-12-19', 6),
(7, '2022-12-16', 7),
(8, '2022-12-26', 8),
(10, '2022-12-15', 10);

-- --------------------------------------------------------

--
-- Structure de la vue `lesproduitcommande`
--
DROP TABLE IF EXISTS `lesproduitcommande`;

DROP VIEW IF EXISTS `lesproduitcommande`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lesproduitcommande`  AS SELECT `produit`.`nom` AS `nom`, sum(`commande`.`quantite`) AS `quantite` FROM (`commande` join `produit` on((`commande`.`idproduit` = `produit`.`idproduit`))) GROUP BY `produit`.`nom` ORDER BY `produit`.`nom` ASC ;

-- --------------------------------------------------------

--
-- Structure de la vue `prepacommande`
--
DROP TABLE IF EXISTS `prepacommande`;

DROP VIEW IF EXISTS `prepacommande`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prepacommande`  AS SELECT `p`.`nom` AS `nom`, count(`pr`.`idproduit`) AS `nbproduitprepa` FROM (`preparateur` `p` join `preparation` `pr` on((`p`.`idprepa` = `pr`.`idprepa`))) GROUP BY `p`.`nom` ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idacheteur`) REFERENCES `acheteur` (`idacheteur`),
  ADD CONSTRAINT `commande_ibfk_2` FOREIGN KEY (`idproduit`) REFERENCES `produit` (`idproduit`);

--
-- Contraintes pour la table `preparation`
--
ALTER TABLE `preparation`
  ADD CONSTRAINT `preparation_ibfk_1` FOREIGN KEY (`idprepa`) REFERENCES `preparateur` (`idprepa`),
  ADD CONSTRAINT `preparation_ibfk_2` FOREIGN KEY (`idproduit`) REFERENCES `produit` (`idproduit`);

--
-- Contraintes pour la table `registre`
--
ALTER TABLE `registre`
  ADD CONSTRAINT `registre_ibfk_1` FOREIGN KEY (`idproduit`) REFERENCES `produit` (`idproduit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;





1)
SELECT COUNT( DISTINCT `idacheteur`)
FROM commande


2)
SELECT COUNT(DISTINCT a.ville)
FROM commande c
INNER JOIN acheteur a on c.idacheteur=a.idacheteur 


3)
SELECT DISTINCT(produit.nom)
FROM commande
INNER JOIN produit on commande.idproduit=produit.idproduit
ORDER BY produit.nom ASC

4)
SELECT SUM(quantite)
FROM commande

5)
SELECT produit.nom,Sum(`quantite`)
FROM commande
INNER JOIN produit on commande.idproduit=produit.idproduit
GROUP by produit.nom
ORDER BY produit.nom ASC

6)
CREATE VIEW lesproduitcommande as 
SELECT produit.nom,Sum(`quantite`) as quantite 
FROM commande
INNER JOIN produit on commande.idproduit=produit.idproduit
GROUP by produit.nom
ORDER BY produit.nom ASC

SELECT * FROM `lesproduitcommande` 
WHERE `quantite`=(SELECT MAX(quantite) FROM lesproduitcommande)


7)
SELECT * FROM `lesproduitcommande` 
WHERE `quantite`=(SELECT MIN(quantite) FROM lesproduitcommande)


8)
SELECT p.nom
FROM preparateur p
LEFT JOIN produit pr on p.idprepa=pr.idprepa
WHERE
pr.idprepa is NULL

9)
CREATE VIEW prepacommande as 
SELECT p.nom,count(pr.idproduit) as nbproduitprepa
FROM preparateur p
INNER JOIN produit pr on p.idprepa=pr.idprepa
GROUP by p.nom

SELECT * 
FROM prepacommande 
WHERE nbproduitprepa =(SELECT MAX(nbproduitprepa) FROM prepacommande)



10)
SELECT a.nomenteprise
FROM commande c
right JOIN acheteur a  on c.idacheteur=a.idacheteur
WHERE
c.idacheteur is null

11)
SELECT a.nomenteprise
FROM commande c
inner JOIN acheteur a  on c.idacheteur=a.idacheteur
INNER JOIN produit p on c.idproduit=p.idproduit
WHERE
p.nom='table basse'


12)
SELECT p.nom,re.dateprepa
FROM registre re
INNER JOIN produit p on re.idproduit=P.idproduit
WHERE
p.nom='canapé lit'
OR
P.nom='bureau'

13)
SELECT p.nom
FROM produit p
INNER JOIN registre r ON p.idproduit=r.idproduit
LEFT JOIN commande c on p.idproduit=c.idproduit
WHERE
c.idproduit is NULL

14)

SELECT Distinct(p.nom)
FROM produit p
INNER JOIN commande c on p.idproduit=c.idproduit
LEFT JOIN registre r ON p.idproduit=r.idproduit
WHERE
r.idproduit is NULL


