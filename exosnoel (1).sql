



CREATE TABLE enfant(
   idenfant INT,
   nom VARCHAR(30),
   ville VARCHAR(50),
   passion VARCHAR(50),
   age INT,
   PRIMARY KEY(idenfant)
);   

CREATE TABLE lutin(
   idlutin INT,
   nom VARCHAR(50),
   PRIMARY KEY(idlutin)
);   

CREATE TABLE cadeau(
   idcadeau INT,
   nomcadeau VARCHAR(50),
   idlutin INT NOT NULL,
   PRIMARY KEY(idcadeau),
   FOREIGN KEY(idlutin) REFERENCES lutin(idlutin)
);   

DROP TABLE IF EXISTS `souhait`;
CREATE TABLE IF NOT EXISTS `souhait` (
  `idenfant` int(11) NOT NULL,
  `idcadeau` int(11) NOT NULL,
  `datepreparation` date DEFAULT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`idenfant`,`idcadeau`),
  KEY `idcadeau` (`idcadeau`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--


INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (1,'peluche tortank',1);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (2,'peluche carapuce',1);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (3,'peluche pikachu',1);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (4,'train électrique',3);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (5,'tortues ninja',2);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (6,'la reine des neiges',2);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (7,'jeux de cartes',3);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (8,'cluedo',3);
INSERT INTO `cadeau`(`idcadeau`, `nomcadeau`,`idlutin`) VALUES (9,'barbie',2);

INSERT INTO `enfant`(`idenfant`, `nom`, `age`, `ville`, `passion`) VALUES (1,'Leopold',5,'Carentan','');
INSERT INTO `enfant`(`idenfant`, `nom`, `age`, `ville`, `passion`) VALUES (2,'Mathilde',8,'Strasbourg','choucroute');
INSERT INTO `enfant`(`idenfant`, `nom`, `age`, `ville`, `passion`) VALUES (3,'Kevin',7,'Saint-Marie-du-Mont','');
INSERT INTO `enfant`(`idenfant`, `nom`, `age`, `ville`, `passion`) VALUES (4,'Martin',9,'Sainte-Mère-Église','');

INSERT INTO `lutin`(`idlutin`, `nom`) VALUES (1,'Tom');
INSERT INTO `lutin`(`idlutin`, `nom`) VALUES (2,'Eva');
INSERT INTO `lutin`(`idlutin`, `nom`) VALUES (3,'Charlotte');
INSERT INTO `lutin`(`idlutin`, `nom`) VALUES (4,'Franky');




INSERT INTO `souhait` (`idenfant`, `idcadeau`, `datepreparation`, `quantite`) VALUES
(1, 4, '2022-12-20', 2),
(1, 5, '2022-12-20', 5),
(2, 4, '2022-12-23', 1),
(2, 6, '2022-12-21', 3),
(2, 7, '2022-12-23', 2),
(2, 3, '2022-12-22', 6),
(4, 6, '2022-12-22', 2),
(4, 9, '2022-12-23', 4),
(4, 5, '2022-12-20', 1),
(4, 8, '2022-12-19', 2),
(3, 1, '2022-12-22', 1),
(3, 2, '2022-12-23', 6),
(6, 11, NULL, 2);





          
1)
SELECT  COUNT(DISTINCT(`idenfant`))
FROM souhait

2)
SELECT  DISTINCT(`ville`)
FROM enfant
ORDER BY `ville` DESC

3)
SELECT DISTINCT(`nomcadeau`)
FROM cadeau c
INNER JOIN souhait s on c.idcadeau=s.idcadeau
ORDER BY `nomcadeau` Asc 

4)
SELECT SUM(s.quantite)
FROM cadeau c
INNER JOIN souhait s
ON
c.idcadeau=s.idcadeau
ORDER BY `nomcadeau` Asc 


5)
CREATE VIEW lesnombrecommandes as 
SELECT nomcadeau,SUM(s.quantite) AS nombredesouhait
          FROM cadeau c
            INNER JOIN souhait s
            ON
            c.idcadeau=s.idcadeau
          group BY nomcadeau         

 6)         

SELECT * 
FROM `lesnombrecommandes`
WHERE 
`nombredesouhait`=(SELECT MAX(`nombredesouhait`) FROM `lesnombrecommandes`)


7)

SELECT * 
FROM `lesnombrecommandes`
WHERE 
`nombredesouhait`=(SELECT MIN(`nombredesouhait`) FROM `lesnombrecommandes`)




8)
SELECT l.nom,COUNT(s.idcadeau) as nombrexemplaire
FROM lutin l
inner JOIN cadeau c  on c.idlutin=l.idlutin
INNER JOIN souhait s on c.idcadeau=s.idcadeau
GROUP by l.nom 
ORDER by nombrexemplaire Desc limit 1

9)
SELECT nom 
FROM lutin l 
LEFT JOIN cadeau c on c.idlutin=l.idlutin
WHERE
c.idlutin is NULL

10)
SELECT nom 
FROM enfant e
LEFT JOIN souhait s on e.idenfant=s.idenfant
WHERE
s.idenfant is NULL

11)
SELECT  distinct(nom)
FROM enfant e
INNER JOIN  souhait s on  e.idenfant=s.idenfant
INNER JOIN cadeau c on s.idcadeau=c.idcadeau
WHERE
nomcadeau like "peluche%"

12)
SELECT datepreparation,nomcadeau
FROM souhait s 
INNER JOIN cadeau c on s.idcadeau=c.idcadeau
WHERE
nomcadeau="train électrique"
or 
nomcadeau="barbie"

13)
SELECT nomcadeau
FROM  cadeau c
LEFT JOIN souhait s on c.idcadeau=s.idcadeau
WHERE
s.idcadeau is NULL

14)
SELECT nomcadeau
FROM  cadeau c
INNER JOIN souhait s on c.idcadeau=s.idcadeau
WHERE
s.datepreparation is NULL










    





















