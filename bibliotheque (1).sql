-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 29 nov. 2022 à 15:58
-- Version du serveur : 5.7.24
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `auteurs`
--

DROP TABLE IF EXISTS `auteurs`;
CREATE TABLE IF NOT EXISTS `auteurs` (
  `idauteur` int(11) NOT NULL,
  `nomauteur` varchar(50) DEFAULT NULL,
  `datenaissance` date DEFAULT NULL,
  `bio` text,
  `datedeces` date DEFAULT NULL,
  PRIMARY KEY (`idauteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `auteurs`
--

INSERT INTO `auteurs` (`idauteur`, `nomauteur`, `datenaissance`, `bio`, `datedeces`) VALUES
(1, 'Stephen King ', '2022-11-23', 'Stephen King [ˈstiːvən kɪŋ]N 1, né le 21 septembre 1947 à Portland dans le Maine, est un écrivain américain. Il publie son premier roman en 1974 et devient rapidement célèbre pour ses contributions dans le domaine de l\'horreur mais écrit également des livres relevant d\'autres genres comme le fantastique, la fantasy, la science-fiction et le roman policier. Tout au long de sa carrière, il écrit et publie plus de soixante romans, dont sept sous le nom de plume de Richard Bachman, et plus de deux cents nouvelles, dont plus de la moitié sont réunies dans douze recueils de nouvelles. Après son grave accident en 1999, il ralentit son rythme d\'écriture. Ses livres se sont vendus à plus de 350 millions d\'exemplaires à travers le monde1 et il établit de nouveaux records de ventes dans le domaine de l\'édition durant les années 1980, décennie où sa popularité atteint son apogée. Longtemps dédaigné par les critiques littéraires et les universitaires car considéré comme un auteur « populaire », il acquiert plus de considération depuis les années 1990 même si une partie de ces milieux continue de rejeter ses livres. Il est régulièrement critiqué pour son style familier, son recours au gore et la longueur jugée excessive de certains de ses romans. À l\'inverse, son sens de la narration, ses personnages vivants et colorés, et sa faculté à jouer avec les peurs des lecteurs sont salués. Au-delà du caractère horrifique de la plupart de ses livres, il aborde régulièrement les thèmes de l\'enfance et de la condition de l\'écrivain, et brosse un portrait social très réaliste et sans complaisance des États-Unis à la fin du XXe siècle et au début du siècle suivant en abordant des sujets de société comme l\'homophobie, le fanatisme religieux et la religion en général, le racisme, le harcèlement, etc. Stephen King a remporté de nombreux prix littéraires dont treize fois le prix Bram-Stoker, sept fois le prix British Fantasy, cinq fois le prix Locus, quatre fois le prix World Fantasy, deux fois le prix Edgar-Allan-Poe, une fois le prix Hugo, l\'O. Henry Award et le prestigieux Grand Master Award des Mystery Writers of America pour l\'ensemble de sa carrière en 2007. Il a reçu en 2003 le National Book Award pour sa remarquable contribution à la littérature américaine. Il a été décoré de la National Medal of Arts en 2015, directement des mains du président américain Barack Obama à la Maison Blanche2. Ses ouvrages ont souvent été adaptés pour le cinéma ou la télévision avec plus ou moins de succès, parfois avec sa contribution en tant que scénariste et, une seule fois, comme réalisateur.', NULL),
(2, 'Agatha Christie', '2022-11-10', 'Agatha Christie est une femme de lettres britannique, auteur de nombreux romans policiers, née le 15 septembre 1890 à Torquay, Royaume-Uni, et morte le 12 janvier 1976 à Wallingford au Royaume-Uni. Son nom de plume est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ». En effet, Agatha Christie est l\'une des écrivaines les plus importantes et novatrices du genre policier. Elle a aussi écrit plusieurs romans, dont quelques histoires sentimentales, sous le pseudonyme de Mary Westmacott. Agatha Christie fait partie des écrivains les plus connus au monde et elle est considérée comme l\'auteur le plus lu de l\'histoire chez les Anglo-Saxons, après William Shakespeare ; c\'est aussi de très loin l\'auteur le plus traduit dans le monde2. Elle a publié 66 romans, 154 nouvelles et 20 pièces de théâtre, ces œuvres ayant été traduites en plusieurs langues. La plupart des intrigues se déroulent à huis clos, ce qui permet au lecteur d\'essayer de deviner l\'identité du coupable avant la fin du récit. La saveur de ses histoires réside dans la résolution de l\'enquête, souvent improbable, prenant le lecteur par surprise. Ses romans et nouvelles ont été adaptés au cinéma, dans des jeux vidéo ou à la télévision, en particulier Le Crime de l\'Orient-Express, Dix Petits Nègres, Mort sur le Nil, Le Train de 16 h 50 et Témoin à charge.', '2022-11-22'),
(3, 'Honoré de Balzac', '2022-11-15', ' Honoré de Balzac, né Honoré Balzacn 1 le 20 mai 1799 (1er prairial an VII du calendrier républicain) à Tours et mort le 18 août 1850 (à 51 ans) à Paris, est un écrivain français. Romancier, dramaturge, critique littéraire, critique d\'art, essayiste, journaliste et imprimeur, il a laissé l\'une des plus imposantes œuvres romanesques de la littérature française, avec plus de quatre-vingt-dix romans et nouvelles parus de 1829 à 1855, réunis sous le titre de La Comédie humaine. À cela s\'ajoutent Les Cent Contes drolatiques, ainsi que des romans de jeunesse publiés sous des pseudonymes et quelque vingt-cinq œuvres ébauchées. Il est un maître du roman français, dont il a abordé plusieurs genres, du roman philosophique avec Le Chef-d\'œuvre inconnu au roman fantastique avec La Peau de chagrin ou encore au roman poétique avec Le Lys dans la vallée. Il a surtout excellé dans la veine du réalisme, avec notamment Le Père Goriot et Eugénie Grandet. Comme il l\'explique dans son avant-propos à La Comédie humaine, il a pour projet d\'identifier les « espèces sociales » de son époque, tout comme Buffon avait identifié les espèces zoologiques. Ayant découvert par ses lectures de Walter Scott que le roman pouvait aspirer à une « valeur philosophique », il veut explorer les différentes classes sociales et les individus qui les composent afin d\'« écrire l\'histoire oubliée par tant d\'historiens, celle des mœurs » et de « faire concurrence à l\'état civil ». L\'auteur décrit la montée du capitalisme, l\'essor de la bourgeoisie face à la noblesse, dans une relation complexe faite de mépris et d\'intérêts communs. Intéressé par les êtres qui ont un destin, il crée des personnages plus grands que nature : « Chacun, chez Balzac, même les portières, a du génie » (Baudelaire). Ses opinions politiques sont ambiguës : s\'il affiche des convictions légitimistes en pleine monarchie de Juillet, il s\'est auparavant déclaré libéral et défendra les ouvriers en 1840 et en 1848, même s\'il ne leur accorde aucune place dans ses romans. Tout en professant des idées conservatrices, il a produit une œuvre admirée par Marx et Engels, et qui invite par certains aspects à l\'anarchisme et à la révolte. Outre sa production littéraire, il a écrit des articles dans les journaux et a dirigé successivement deux revues, qui feront faillite. Convaincu de la haute mission de l\'écrivain, qui doit régner par la pensée, il lutte pour le respect des droits d\'auteur et contribue à la fondation de la Société des gens de lettres. Travailleur forcené, fragilisant par ses excès une santé précaire, endetté à la suite d\'investissements hasardeux et d\'excès somptuaires, fuyant ses créanciers sous de faux noms dans différentes demeures, Balzac a aussi eu de nombreuses liaisons amoureuses avant d\'épouser en 1850 la comtesse Hańska, qu\'il avait courtisée pendant dix-sept ans. Comme l\'argent qu\'il gagnait avec sa plume ne suffisait pas à payer ses dettes, il avait sans cesse en tête des projets mirobolants : une imprimerie, un journal, une mine d\'argent. C\'est dans un palais situé rue Fortunée qu\'il meurt profondément endetté au milieu d\'un luxe inouï. Lu et admiré dans toute l\'Europe, Balzac a fortement influencé les écrivains de son temps et du siècle suivant. Le roman L\'Éducation sentimentale de Gustave Flaubert est directement inspiré du Lys dans la vallée, et Madame Bovary, de La Femme de trente ans[réf. nécessaire]. Le principe du retour de personnages évoluant et se transformant au sein d\'un vaste cycle romanesque a notamment inspiré Émile Zola (1840 ; 1902) , Guy de Maupassant (1850 ; 1893) et Marcel Proust (1871 ; 1922). Ses œuvres continuent d\'être rééditées. Le cinéma a adapté La Marâtre dès 1906 ; depuis, les adaptations cinématographiques et télévisuelles de cette œuvre immense se sont multipliées, avec plus d\'une centaine de films et de téléfilms produits à travers le monde.', '2022-11-07');

-- --------------------------------------------------------

--
-- Structure de la table `editeurs`
--

DROP TABLE IF EXISTS `editeurs`;
CREATE TABLE IF NOT EXISTS `editeurs` (
  `idediteur` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  `ville` varchar(60) DEFAULT NULL,
  `pays` varchar(30) DEFAULT NULL,
  `telephone` varchar(14) DEFAULT NULL,
  `fax` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`idediteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `editeurs`
--

INSERT INTO `editeurs` (`idediteur`, `nom`, `adresse`, `code`, `ville`, `pays`, `telephone`, `fax`) VALUES
(1, 'Hachette', '5 rue de la villette', '75000', 'Paris', 'france', ' 0158965698 ', '0158745896'),
(2, 'Editis', '10 rue de la marne', '75210', 'paris', 'france', '0125635987', '0125635698'),
(3, 'Galimard ', '1 rue de la tuile ', '75000', 'Paris ', 'france', ' 0125635658 ', ' 0124589654');

-- --------------------------------------------------------

--
-- Structure de la table `emprunteur`
--

DROP TABLE IF EXISTS `emprunteur`;
CREATE TABLE IF NOT EXISTS `emprunteur` (
  `idemprunteur` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `ville` varchar(60) DEFAULT NULL,
  `telephone` char(14) DEFAULT NULL,
  `sexe` char(1) DEFAULT NULL,
  `datenaiss` date DEFAULT NULL,
  `nbretard` int(11) DEFAULT NULL,
  PRIMARY KEY (`idemprunteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `emprunteur`
--

INSERT INTO `emprunteur` (`idemprunteur`, `nom`, `prenom`, `adresse`, `code`, `ville`, `telephone`, `sexe`, `datenaiss`, `nbretard`) VALUES
(1, 'Renard ', 'Jules', 'rue de la vallée', ' 22000 ', 'Saint-Brieuc', ' 0298635698', 'h', '1969-12-12', 1),
(2, 'Dubois ', 'Julie', '4 allée de la marne', ' 22360 ', 'Langueux', '0284699875', 'f', '2022-11-03', 0),
(3, 'Tardivel ', 'Paul ', '7 bis rue de la canne ', ' 22000 ', 'Saint-Brieuc', '02.65.45.23.36', 'h', '1978-04-14', 3),
(4, 'Corcuff', 'Anaël', '1 rue de la baie des pierres', '22000 ', 'Saint-Brieuc', '02.12.12.36.56', 'f', '2022-11-02', 0);

-- --------------------------------------------------------

--
-- Structure de la table `emprunts`
--

DROP TABLE IF EXISTS `emprunts`;
CREATE TABLE IF NOT EXISTS `emprunts` (
  `idemprunt` int(11) NOT NULL,
  `datepret` date DEFAULT NULL,
  `daterendu` date DEFAULT NULL,
  `idemprunteur` int(11) NOT NULL,
  `idlivres` int(11) NOT NULL,
  PRIMARY KEY (`idemprunt`),
  KEY `idemprunteur` (`idemprunteur`),
  KEY `idlivres` (`idlivres`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `emprunts`
--

INSERT INTO `emprunts` (`idemprunt`, `datepret`, `daterendu`, `idemprunteur`, `idlivres`) VALUES
(1, '2022-11-25', '2022-11-28', 1, 1),
(2, '2022-12-12', NULL, 1, 6),
(3, NULL, NULL, 1, 10),
(4, '2022-08-22', NULL, 1, 4),
(5, '2022-09-12', '2022-10-09', 2, 12),
(6, '2022-11-22', NULL, 2, 9),
(7, '2022-09-12', '2022-10-09', 2, 3),
(9, '2019-01-15', '2022-02-12', 3, 5),
(10, '2022-11-28', NULL, 3, 13),
(11, '2022-09-05', '2022-09-02', 3, 11),
(12, '2022-03-01', '2022-03-17', 3, 8);

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

DROP TABLE IF EXISTS `livres`;
CREATE TABLE IF NOT EXISTS `livres` (
  `idlivres` int(11) NOT NULL,
  `isbn` varchar(13) DEFAULT NULL,
  `titre` varchar(100) DEFAULT NULL,
  `nbpages` int(11) DEFAULT NULL,
  `dateparu` date DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `theme` varchar(30) DEFAULT NULL,
  `format` varchar(15) DEFAULT NULL,
  `idauteur` int(11) NOT NULL,
  `idediteur` int(11) NOT NULL,
  PRIMARY KEY (`idlivres`),
  KEY `idauteur` (`idauteur`),
  KEY `idediteur` (`idediteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `livres`
--

INSERT INTO `livres` (`idlivres`, `isbn`, `titre`, `nbpages`, `dateparu`, `prix`, `theme`, `format`, `idauteur`, `idediteur`) VALUES
(1, '15456', 'Billy Summer ', 322, '2015-11-11', 22, 'Horreur/thriller', 'Poche', 1, 1),
(2, '15457', 'Après', 356, '2014-11-05', 28, 'Horreur/thriller', 'Poche', 1, 1),
(3, '15458', 'Shining', 292, '2014-11-05', 21, 'Horreur/thriller', 'Poche', 1, 2),
(4, '15459', 'Sac d\'os', 598, '2015-11-04', 28, 'Horreur/thriller', 'Roman', 1, 3),
(5, '15460', 'Salem', 423, '2016-11-02', 25, 'Horreur/thriller', 'Roman', 1, 3),
(6, '15431', 'Le crime parfait ', 256, '2022-11-09', 22, 'policier', 'Poche', 2, 2),
(7, '15461', 'Le crime de l\'Orient Express ', 2786, '2014-11-05', 25, 'policier', 'Roman', 2, 2),
(8, '15490', 'Mort sur le Nil', 312, '2022-11-16', 21, 'policier', 'poche', 2, 2),
(9, '15447', 'Hercule Poirot quitte la scène', 455, '2022-11-30', 28, 'policier', 'Roman', 2, 2),
(10, '1478', 'Le père Goriot', 256, '2022-11-24', 22, 'fiction ', 'poche', 3, 3),
(11, '15470', 'Illusions perdues ', 246, '2014-11-04', 28, 'fiction ', 'roman', 3, 1),
(12, '1457', 'Eugène Grandet', 356, '2022-11-08', 22, 'fiction ', 'poche', 3, 1),
(13, '1475', 'Le Lys dans la vallée', 253, '2022-11-08', 24, 'fiction ', 'Roman', 3, 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `emprunts`
--
ALTER TABLE `emprunts`
  ADD CONSTRAINT `emprunts_ibfk_1` FOREIGN KEY (`idemprunteur`) REFERENCES `emprunteur` (`idemprunteur`),
  ADD CONSTRAINT `emprunts_ibfk_2` FOREIGN KEY (`idlivres`) REFERENCES `livres` (`idlivres`);

--
-- Contraintes pour la table `livres`
--
ALTER TABLE `livres`
  ADD CONSTRAINT `livres_ibfk_1` FOREIGN KEY (`idauteur`) REFERENCES `auteurs` (`idauteur`),
  ADD CONSTRAINT `livres_ibfk_2` FOREIGN KEY (`idediteur`) REFERENCES `editeurs` (`idediteur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


SELECT nomauteur,titre FROM auteurs a INNER join livres l on a.idauteur=l.idauteur;

SELECT nom,prenom,datepret
FROM emprunteur em
INNER join emprunts e on em.idemprunteur=e.idemprunteur


Select titre,nomauteur,nom 
FROM emprunteur em
INNER join emprunts e on em.idemprunteur=e.idemprunteur
INNER join livres l on e.idlivres=l.idlivres
INNER join auteurs a on a.idauteur=l.idauteur;

SELECT idemprunt,nom 
FROM emprunteur em
INNER join emprunts e on em.idemprunteur=e.idemprunteur


SELECT idemprunt,nom 
FROM emprunteur em
left join emprunts e on em.idemprunteur=e.idemprunteur
WHERE 
e.idemprunteur IS not NULL
or
em.idemprunteur not in(
    Select e.idemprunteur
    FROM emprunts e 
)



SELECT idemprunt,e.idemprunteur,nom 
FROM emprunteur em
left join emprunts e on em.idemprunteur=e.idemprunteur
WHERE 
e.idemprunteur IS  NULL
or
em.idemprunteur not in(
    Select e.idemprunteur
    FROM emprunts e 
)


SELECT  titre,idemprunteur,nomauteur,nom
FROM livres l
Inner join emprunts e on l.idlivres=e.idlivres
INNER join auteurs a on l.idauteur=a.idauteur
INNER join editeurs ed on l.idediteur=ed.idediteur
WHERE
datepret LIKE "2022%"






